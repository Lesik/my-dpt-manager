import { defineConfig } from "vite";
import react from '@vitejs/plugin-react';

export default defineConfig({
    plugins: [
      react()
    ],
    server: {
        proxy: {
            '^/discover': 'http://localhost:3021',
            '^/information': 'http://localhost:3021',
            '^/authcookie': 'http://localhost:3021',
            '^/proxy': 'http://localhost:3021',
        },
    },
});
