const { createProxyMiddleware } = require('http-proxy-middleware');

const proxyMiddleware = createProxyMiddleware({
    target: 'http://localhost:3021',
    changeOrigin: true,
  });

module.exports = function (app) {
  app.use('^/discover', proxyMiddleware);
  app.use('^/information', proxyMiddleware);
  app.use('^/authcookie', proxyMiddleware);
  app.use('^/proxy', proxyMiddleware);
};
