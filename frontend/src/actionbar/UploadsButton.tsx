import { Badge, Divider, IconButton, LinearProgress, Popover, Stack, Tooltip, Typography } from "@mui/material";
import { Cancel, CheckCircle, Download } from "@mui/icons-material";
import { useWithAnchor } from "../hooks/useWithAnchor";
import { useUploadingFiles } from "../redux/uploadFilesHook";
import { UploadingFile } from "../types";
import classes from './UploadsButton.module.css';

export const UploadsButton = () => {
    const { files: uploads } = useUploadingFiles();
    const { anchor, isOpen, openMenu, closeMenu } = useWithAnchor<HTMLButtonElement>();

    if (uploads.length === 0) {
        return null;
    }

    return (
        <>
            <IconButton onClick={openMenu} color="inherit">
                <Badge badgeContent={uploads.length} color="secondary">
                    <Download />
                </Badge>
            </IconButton>
            <Popover
                open={isOpen}
                anchorEl={anchor}
                onClose={closeMenu}
                anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'left',
                }}
            >
                <Stack divider={<Divider />}>
                    {uploads.map(uploadStatus => <UploadEntry file={uploadStatus} key={uploadStatus.document_id} />)}
                </Stack>
            </Popover>
        </>
    );
};

export const UploadEntry = ({ file }: { file: UploadingFile }) => {
    const complete = file.progress === 100;

    return (
        <div className={classes.uploadEntry}>
            <Typography children={file.fileName} className={classes.name} />
            <LinearProgress variant="determinate" value={file.progress} />
            <Typography>
                {complete ? 'Complete!' : `Uploading... ${file.progress}% complete`}
            </Typography>
            <Tooltip title="Stop upload">
                <IconButton disabled={complete}>
                    {
                        complete ? <CheckCircle /> : <Cancel />
                    }
                </IconButton>
            </Tooltip>
        </div>
    );
};

