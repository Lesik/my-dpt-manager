import { AppBar, Box, Toolbar, Typography } from "@mui/material";
import React from "react";
import { UploadsButton } from "./UploadsButton";

export const ActionBar = () => {
    return (
        <AppBar position="static">
            <Toolbar>
                <Box sx={{ flexGrow: 1 }}>
                    <Typography variant="h5">File manager</Typography>
                </Box>
                <UploadsButton />
            </Toolbar>
        </AppBar>
    );
};
