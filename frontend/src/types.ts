export type UploadingFile = {
    fileName: string,
    progress?: number,
    document_id: string,
    completed: boolean,
}
