import { createContext, PropsWithChildren, useContext } from "react";

export function createKVStore<T>(displayName: string) {
    const Context = createContext<T>({} as T);
    Context.displayName = displayName;
    const Provider = Context.Provider;

    return {
        Context,
        useContext: () => {
            const context = useContext(Context);
            if (Object.keys(context).length === 0) {
                throw new Error(`${displayName} is empty, did you forget to wrap with <${displayName}.Provider>?`);
            }
            return context;
        },
        Provider: (props: PropsWithChildren<T>) => (
            <Provider value={props}>
                {props.children}
            </Provider>
        ),
    };
};
