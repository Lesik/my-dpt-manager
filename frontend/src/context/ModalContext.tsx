import { createContext, PropsWithChildren, useState } from "react";
import { useContextSafely } from "../helpers/react";

type ModalType = JSX.Element;

type ModalContextType = {
    isModalShown: boolean,
    showModal: (modal: ModalType) => void,
    hideModal: () => void,
    killModal: () => void,
};

const ModalContext = createContext<ModalContextType>({} as ModalContextType);
ModalContext.displayName = 'ModalContext';

export const useModal = () => useContextSafely(ModalContext);

export const ModalContextProvider = ({ children }: PropsWithChildren<{}>) => {
    const [modal, setModal] = useState<ModalType>();
    const [isModalShown, setModalShows] = useState(false);

    const contextValue: ModalContextType = {
        isModalShown,
        showModal: (newModal: ModalType) => {
            setModal(newModal);
            setModalShows(true);
        },
        hideModal: () => {
            setModalShows(false);
        },
        killModal: () => setModal(undefined),
    };

    return (
        <ModalContext.Provider value={contextValue}>
            { children }
            { modal }
        </ModalContext.Provider>
    );
}
