import React, { Dispatch, ReactNode, SetStateAction, useMemo, useState } from "react";
import axios, { AxiosInstance } from "axios";
import { createKVStore } from "./KVStoreFactory";
import { WelcomeView } from "../views/WelcomeView";

export type Device = {
    ip: string,
}

const {
    useContext: useDeviceSetupContext,
    Provider: SetupContext,
} = createKVStore<{
    setDevice: Dispatch<SetStateAction<Device | undefined>>,
}>('DeviceSetup');

/**
 * This component will only show its children if a device connection is established. Otherwise it will show the setup.
 */
export const DeviceConnectionContextProvider = ({ children }: { children: ReactNode }) => {
    const [device, setDevice] = useState<Device>();

    if (!device) {
        return (
            <SetupContext setDevice={setDevice}>
                <WelcomeView />
            </SetupContext>
        );
    }

    return <ConnectionProvider device={device} children={children} />;
};

export { useDeviceSetupContext };

const {
    useContext: useDeviceConnection,
    Provider: InternalConnectionProvider,
} = createKVStore<{
    device: Device,
    axiosInst: AxiosInstance,
}>('DeviceConnection');

const ConnectionProvider = ({ device, children }: { device: Device, children: ReactNode }) => {
    const axiosInst = useMemo(() => axios.create({
        baseURL: `/proxy/${device.ip}`,
    }), [device]);
    return (
        <InternalConnectionProvider
            device={device}
            axiosInst={axiosInst}
            children={children}
        />
    );
};

export { ConnectionProvider, useDeviceConnection };
