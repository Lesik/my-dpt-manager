import { MouseEvent, useState } from "react";

export const useWithAnchor = <T = HTMLElement>() => {
    const [anchor, setAnchor] = useState<T>();

    const openMenu = (event: MouseEvent<T>) => setAnchor(event.currentTarget);
    const closeMenu = () => setAnchor(undefined);
    // const toggleMenu = (event: MouseEvent<T>) => anchor === undefined ? openMenu(event) : closeMenu();

    return {
        anchor,
        isOpen: anchor !== undefined,
        openMenu,
        closeMenu,
    };
};
