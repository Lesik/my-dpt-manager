import { useParams } from "react-router-dom";

export const useCurrentFolder = () => {
    const { id = 'root' } = useParams();
    return id;
}
