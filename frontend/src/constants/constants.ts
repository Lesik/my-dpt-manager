import { Dispatch, SetStateAction } from "react";

export enum ActionType {
    NEGATIVE,
    WARNING,
    POSITIVE,
    NEUTRAL,
}

export type UseState<T> = [T, Dispatch<SetStateAction<T>>];
