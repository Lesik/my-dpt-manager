import { Refresh, Error } from "@mui/icons-material";
import { ListItem, ListItemAvatar, Avatar, ListItemText, ListItemSecondaryAction, IconButton } from "@mui/material";

export const ErrorDevice = ({ onClick }: { onClick: () => any }) => (
    <ListItem onClick={onClick}>
        <ListItemAvatar>
            <Avatar>
                <Error />
            </Avatar>
        </ListItemAvatar>
        <ListItemText
            primary="Error communicating with the device"
            secondary="Try again?"
        />
        <ListItemSecondaryAction>
            <IconButton onClick={onClick}>
                <Refresh />
            </IconButton>
        </ListItemSecondaryAction>
    </ListItem>
);
