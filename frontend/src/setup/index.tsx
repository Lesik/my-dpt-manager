import { Box, DialogTitle, Divider } from "@mui/material";
import React from "react";
import { DeviceList } from "./DeviceList";

export const Setup = () => (
    <Box display="flex" height="100%" flexDirection="column">
        <DialogTitle>Select device</DialogTitle>
        <Divider />
        <DeviceList />
    </Box>
);
