import { TabletAndroid } from "@mui/icons-material";
import { Avatar, CircularProgress, ListItem, ListItemAvatar, ListItemText } from "@mui/material";
import React from "react";
import { ErrorDevice } from "./DeviceListEntryError";
import { UnknownDevice } from "./DeviceListEntryUnknown";
import { useDeviceInformation } from "../queries/devices/getDeviceInformation";
import { useAuthCookie } from "../queries/devices/getAuthCookie";


export function DeviceListEntry({
    ip,
    selected,
    setSelected,
}: {
    ip: string,
    selected: boolean,
    setSelected: React.Dispatch<React.SetStateAction<string | null>>,
}) {
    const query = useDeviceInformation(ip);
    const authCookieQuery = useAuthCookie(selected, ip)

    if (query.isLoading) {
        return <UnknownDevice selected={selected} />;
    }

    if (query.isError || query.isIdle) {
        return <ErrorDevice onClick={() => query.refetch()} />;
    }

    return (
        <ListItem
            button
            selected={selected}
            onClick={
                // if already selected and user clicks again, probably query failed
                () => selected ? authCookieQuery.refetch() : setSelected(ip)
            }
        >
            <ListItemAvatar>
                <Avatar>
                    {
                        authCookieQuery.isLoading ? <CircularProgress /> : <TabletAndroid />
                    }
                </Avatar>
            </ListItemAvatar>
            <ListItemText
                primary={query.data.modelName}
                secondary={`Serial number: ${query.data.serialNumber}`}
            />
        </ListItem>
    );
}
