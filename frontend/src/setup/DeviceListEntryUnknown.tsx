import { Help } from "@mui/icons-material";
import { Avatar, ListItem, ListItemAvatar, ListItemText } from "@mui/material";

export const UnknownDevice = ({ selected }: { selected: boolean }) => (
    <ListItem selected={selected} disabled>
        <ListItemAvatar>
            <Avatar>
                <Help />
            </Avatar>
        </ListItemAvatar>
        <ListItemText
            primary="Unknown device"
            secondary="Fetching device information..."
        />
    </ListItem>
);
