import { SentimentVeryDissatisfied } from "@mui/icons-material";
import { Button, CircularProgress, List, Typography } from "@mui/material";
import React, { useState } from "react";
import { CenterBox } from "../components/containers/CenterBox";
import { DeviceListEntry } from "./DeviceListEntry";
import { useDiscoveredDevices } from "../queries/devices/discoverDevices";

export function DeviceList() {
    // TODO this would be better served by a websocket
    const query = useDiscoveredDevices();
    const [selected, setSelected] = useState<string | null>(null);

    if (query.isError) {
        return (
            <CenterBox direction="column">
                <SentimentVeryDissatisfied fontSize="large" />
                <Typography>Sorry! No devices found.</Typography>
                <Button onClick={() => query.refetch()}>Try again?</Button>
            </CenterBox>
        );
    }
    if (query.isSuccess) {
        const ips = [query.data.ip];   // fake it 'til you make it

        return (
            <>
                {
                    ips.map((ip) => (
                        <List key={ip} disablePadding>
                            <DeviceListEntry ip={ip} selected={ip === selected} setSelected={setSelected} />
                        </List>
                    ))
                }
            </>
        );
    }
    if (query.isLoading) {
        return (
            <CenterBox direction="column">
                <CircularProgress />
            </CenterBox>
        );
    }
    return (
        <div>
            something else:
            <pre>
                {JSON.stringify(query)}
            </pre>
        </div>
    );
}
