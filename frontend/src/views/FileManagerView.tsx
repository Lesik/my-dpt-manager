import { Box, CircularProgress, Divider, List } from "@mui/material";
import React, { Fragment, useMemo } from "react";
import { CenterBox } from "../components/containers/CenterBox";
import { Breadcrumbs as MyBreadcrumbs } from '../filemanager/topbar/Breadcrumbs';
import { DocumentEntry, DocumentEntryType } from "../filemanager/DocumentEntry";
import { FileDrop2 } from "../filemanager/FileDrop2";
import { FileManagerEmpty } from "../filemanager/FileManagerEmpty";
import { FolderEntry, FolderEntryType } from "../filemanager/FolderEntry";
import { RefetchingIndicator } from "../helpers/RefetchingIndicator";
import { useFiles } from "../queries/files/getFiles";
import { ModalContextProvider } from "../context/ModalContext";
import { useCurrentFolder } from "../hooks/useCurrentFolder";
import { NewFolderButton } from "../filemanager/topbar/NewFolderButton";
import { RefreshButton } from "../filemanager/topbar/RefreshButton";
import { FileManager404 } from "../filemanager/FileManager404";
import { EntryType } from "../queries/files";

const sortEntries = (entry1: EntryType, entry2: EntryType) => entry1.entry_name.localeCompare(entry2.entry_name);

export function FileManagerView() {
    const id = useCurrentFolder();
    const fileQuery = useFiles();

    const files = useMemo(() => {
        const files = fileQuery.data?.filter(entry => entry.parent_folder_id === id);
        const folders = files?.filter((entry): entry is FolderEntryType => entry.entry_type === 'folder').sort(sortEntries);
        const documents = files?.filter((entry): entry is DocumentEntryType => entry.entry_type === 'document').sort(sortEntries);

        return { folders, documents };
    }, [id, fileQuery.data]);

    if (id !== 'root' && !fileQuery.data?.find(entry => entry.entry_id === id)) {
        return <FileManager404 />;
    }

    if (fileQuery.isIdle) {
        return <></>;
    }

    let Inner = () => {
        if (fileQuery.isSuccess) {
            const { folders, documents } = files;

            if (folders?.length === 0 && documents?.length === 0) {
                return (
                    <FileManagerEmpty />
                );
            }

            return (
                <List
                    disablePadding
                    sx={{
                        display: 'grid',
                        gridAutoRows: '1fr auto',
                    }}
                >
                    {
                        folders?.map(folder => (
                            <Fragment key={folder.entry_id}>
                                <FolderEntry folder={folder} />
                                <Divider />
                            </Fragment>
                        ))
                    }
                    {
                        documents?.map(document => (
                            <Fragment key={document.entry_id}>
                                <DocumentEntry document={document} />
                                <Divider />
                            </Fragment>
                        ))
                    }
                </List>
            );
        }

        return (
            <CenterBox>
                <CircularProgress />
            </CenterBox>
        );
    };

    return (
        <ModalContextProvider>
            <FileDrop2>
                <Box p="10px" display="flex" justifyContent="flex-end" alignItems="center">
                    <MyBreadcrumbs id={id} />
                    <RefreshButton />
                    <NewFolderButton />
                </Box>
                <Divider />
                <RefetchingIndicator query={fileQuery}>
                    <Inner />
                </RefetchingIndicator>
            </FileDrop2>
        </ModalContextProvider>
    );
}
