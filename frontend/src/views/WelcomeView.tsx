import { Box, Dialog } from "@mui/material";
import React, { useState } from "react";
import { Setup } from "../setup";

export function WelcomeView() {
    const [currentStep] = useState(0);

    const CurrentStep = () => {
        switch (currentStep) {
            case 0:
                return <Setup />;
            case 1:
            default:
                return <h1>Hi</h1>;
        }
    };

    return (
        <Dialog open={true} fullWidth>
            <Box height="40vh">
                <CurrentStep />
            </Box>
        </Dialog>
    );
}
