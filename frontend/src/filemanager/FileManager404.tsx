import React from 'react';
import { Box, Button, Typography } from "@mui/material";
import { FolderOff, Home } from "@mui/icons-material";
import { Link } from "react-router-dom";

export const FileManager404 = () => {

    return (
        <Box display="flex" justifyContent="center" alignItems="center" flexDirection="column" height="100%">
            <FolderOff fontSize="large" />
            <Typography variant="h5">This folder does not exist.</Typography>
            <Button startIcon={<Home />} component={Link} to="/">
                Go back to the document root
            </Button>
        </Box>
    );
};
