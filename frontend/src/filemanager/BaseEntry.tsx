import { MoreVert } from "@mui/icons-material";
import { Avatar, IconButton, ListItemAvatar, ListItemText, Menu } from "@mui/material";
import React, { Dispatch, ReactNode, SetStateAction } from "react";
import { useNavigate } from "react-router-dom";
import { UseState } from "../constants/constants";
import { ListItemWithSecondaryAction } from "../helpers/ListItemWithSecondaryAction";

export interface BaseEntryType {
    created_date: string;
    // document_source: string; // not sure what this is
    entry_id: string;
    entry_name: string;
    entry_path: string;
    entry_type: "document" | "folder";
    is_new: "true" | "false";
    parent_folder_id: string;
}

export type MenuItemType = (closeModal: () => void) => ReactNode;

export const BaseEntry = ({
  icon,
  onClick,
  href,
  primary,
  secondary = "",
  menuItems,
}: {
  icon: React.ReactNode,
  onClick?: any,
  href?: string,
  primary: string,
  secondary?: string,
  menuItems?: MenuItemType,
}) => {
  const navigate = useNavigate();
  const anchorState = React.useState<null | HTMLElement>(null);

  let action;
  if (onClick !== undefined) action = onClick;
  if (href !== undefined) action = () => navigate(href);

  return (
      <ListItemWithSecondaryAction
          onClick={action}
          disableRipple
          secondaryAction={menuItems !== undefined && (
              <>
                  <ThreeDotButton anchorState={anchorState} />
                  <ThreeDotMenu anchorState={anchorState} menuItems={menuItems} />
              </>
          )}
      >
          <ListItemAvatar>
              <Avatar>
                  {icon}
              </Avatar>
          </ListItemAvatar>
          <ListItemText
              primary={primary}
              secondary={secondary}
          />
      </ListItemWithSecondaryAction>
  );
};

type AnchorState = [HTMLElement | null, Dispatch<SetStateAction<HTMLElement | null>>];

const ThreeDotButton = ({anchorState: [, setAnchor]}: { anchorState: AnchorState }) => (
  <IconButton onClick={ (e) => setAnchor(e.currentTarget) }>
    <MoreVert/>
  </IconButton>
);

const ThreeDotMenu = ({anchorState: [anchor, setAnchor], menuItems}: {
  anchorState: UseState<HTMLElement | null>,
  menuItems: MenuItemType,
}) => (
  <Menu
    open={ anchor !== null }
    anchorEl={ anchor }
    onClose={ () => setAnchor(null) }
  >
    { menuItems(() => setAnchor(null)) }
  </Menu>
);
