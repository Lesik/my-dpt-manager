import { Description } from "@mui/icons-material";
import React from "react";
import { DeleteAction } from "./actions/DeleteAction";
import { BaseEntry, BaseEntryType } from "./BaseEntry";
import { RenameAction } from "./actions/RenameAction";

export interface DocumentEntryType extends BaseEntryType {
    author?: string; // is really optional
    current_page: string;
    document_type: "normal" | "note";
    entry_type: "document";
    // ext_id?: string; // seems to always be empty string, so I commented it even though it's part of the response
    file_revision?: string; // optional because I don't know how to mock it. doesn't seem optional in backend API
    file_size?: string; // don't care enough to mock this
    mime_type: string;
    // modified_date: string;
    // reading_date: string;
    title: string;
    total_page: string;
};

export const DocumentEntry = ({
    document,
}: {
    document: DocumentEntryType
}) => {
    return (
        <BaseEntry
            icon={<Description />}
            primary={document.entry_name}
            secondary={document.entry_path}
            menuItems={closeModal => [
                <RenameAction key={'rename'} file={document} closeMenu={closeModal} />,
                <DeleteAction key={'delete'} file={document} closeMenu={closeModal} />,
            ]}
        />
    );
};
