import { Folder } from "@mui/icons-material";
import React from "react";
import { BaseEntry, BaseEntryType } from "./BaseEntry";
import { RenameAction } from "./actions/RenameAction";
import { DeleteAction } from "./actions/DeleteAction";

export interface FolderEntryType extends BaseEntryType {
    entry_type: "folder";
    ext_id: string,
}

export const FolderEntry = ({
    folder,
}: {
    folder: FolderEntryType,
}) => {
    return (
        <BaseEntry
            icon={<Folder />}
            href={`/${folder.entry_id}`}
            primary={folder.entry_name}
            menuItems={closeModal => [
                <RenameAction key={'rename'} file={folder} closeMenu={closeModal} />,
                <DeleteAction key={'delete'} file={folder} closeMenu={closeModal} />,
            ]}
        />
    );
};
