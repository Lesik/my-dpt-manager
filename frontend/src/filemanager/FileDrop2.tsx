import { ArrowDropDown, Cancel, SvgIconComponent } from "@mui/icons-material";
import { Icon, Paper, Typography } from "@mui/material";
import { PropsWithChildren } from "react";
import { useDropzone } from 'react-dropzone';
import { ActionType } from "../constants/constants";
import { cl } from "../helpers/react";
import classes from './FileDrop2.module.css';
import { useCurrentFolder } from "../hooks/useCurrentFolder";
import { useNewUploadFile } from "../redux/uploadFilesHook";

export const FileDrop2 = ({ children }: PropsWithChildren<{}>) => {
    const currentFolder = useCurrentFolder();
    const uploadFile = useNewUploadFile();
    const { isDragAccept, isDragActive, getRootProps, getInputProps } = useDropzone({
        noClick: true,
        noKeyboard: true,
        accept: 'application/pdf,.pdf',
        onDrop: (files) => files.map(file => uploadFile(file, currentFolder)),
    });

    return (
        <div {...getRootProps({ className: classes.dropRoot })}>
            <Overlay
                variant={ActionType.NEUTRAL}
                icon={ArrowDropDown}
                text="Drop files here to upload"
                show={isDragAccept}
            />
            <Overlay
                variant={ActionType.NEGATIVE}
                icon={Cancel}
                text="Only PDF files are allowed"
                show={isDragActive && !isDragAccept}
            />
            <input {...getInputProps()} />
            {children}
        </div>
    );
};

const Overlay = ({ icon, text, variant, show }: {
    icon: SvgIconComponent,
    text: string,
    variant: ActionType.NEGATIVE | ActionType.NEUTRAL,
    show: boolean,
}) => (
    <Paper className={cl(classes.overlay, {
        [classes.variantWrongFile]: variant === ActionType.NEGATIVE,
        [classes.variantDropNow]: variant === ActionType.NEUTRAL,
        [classes.show]: show,
    })} sx={{ zIndex: 'tooltip' }}>
        <Icon fontSize="large" component={icon} />
        <Typography variant="h5">{text}</Typography>
    </Paper>
)
