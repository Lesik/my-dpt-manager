import { SvgIconComponent } from "@mui/icons-material";
import { Icon, ListItemIcon, ListItemText, MenuItem } from "@mui/material";
import { ActionType } from "../../constants/constants";

export type MenuItemProp = {
  title: string,
  type: ActionType,
  icon?: SvgIconComponent,
  action?: () => void,
};

enum MuiColors {
  INHERIT = 'inherit',
  DISABLED = 'disabled',
  ACTION = 'action',
  PRIMARY = 'primary',
  SECONDARY = 'secondary',
  ERROR = 'error',
  INFO = 'info',
  SUCCESS = 'success',
  WARNING = 'warning',
}

const typeToColorMap: Record<ActionType, MuiColors> = {
  [ActionType.NEGATIVE]: MuiColors.ERROR,
  [ActionType.NEUTRAL]: MuiColors.INHERIT,
  [ActionType.POSITIVE]: MuiColors.SUCCESS,
  [ActionType.WARNING]: MuiColors.WARNING,
};

export const Action = ({
  title, type, icon, action,
}: MenuItemProp) => (
  <MenuItem key={ title } onClick={ action }>
    { icon === undefined ? null : (
      <ListItemIcon>
        <Icon color={ typeToColorMap[type] } component={ icon }/>
      </ListItemIcon>
    ) }
    <ListItemText { ...(icon === undefined && {inset: true}) }>
      { title }
    </ListItemText>
  </MenuItem>
);
