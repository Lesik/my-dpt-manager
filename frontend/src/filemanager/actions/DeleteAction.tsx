import { Delete } from "@mui/icons-material";
import { ActionType } from "../../constants/constants";
import { Action } from "./Action";
import { useModal } from "../../context/ModalContext";
import { DeleteDialog } from "../dialogs/DeleteDialog";
import { BaseEntryType } from "../BaseEntry";


export const DeleteAction = ({ file, closeMenu }: { file: BaseEntryType, closeMenu: () => void }) => {
    const { showModal } = useModal();

    return (
        <Action
            title={"Delete"}
            type={ActionType.NEGATIVE}
            icon={Delete}
            action={() => {
                closeMenu();
                showModal(<DeleteDialog file={file} />);
            }} />
    );
};

