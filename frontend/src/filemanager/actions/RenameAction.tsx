import { useModal } from "../../context/ModalContext";
import { Action } from "./Action";
import { ActionType } from "../../constants/constants";
import { DriveFileRenameOutline } from "@mui/icons-material";
import { EntryType } from "../../queries/files";
import { RenameDialog } from "../dialogs/RenameDialog";

export const RenameAction = ({ file, closeMenu }: { file: EntryType, closeMenu: () => void }) => {
    const { showModal, killModal } = useModal();

    return (
        <Action title="Rename"
            type={ActionType.NEUTRAL}
            icon={DriveFileRenameOutline}
            action={() => {
                killModal();
                closeMenu();
                showModal(<RenameDialog file={file} />);
            }} />
    );
};
