import { ReactNode } from "react";
import { Button, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle } from "@mui/material";
import { useModal } from "../../context/ModalContext";

export const MyDialog = ({
    title,
    content,
    buttons,
    showCancelButton = true,
}: {
    title: string,
    content: string | ReactNode,
    buttons: ReactNode,
    showCancelButton?: boolean,
}) => {
    const { hideModal, isModalShown } = useModal();

    return (
        <Dialog open={isModalShown} onClose={hideModal}>
            <DialogTitle>
                {title}
            </DialogTitle>
            <DialogContent>
                {
                    typeof content === 'string'
                        ? <DialogContentText>{content}</DialogContentText>
                        : content
                }
            </DialogContent>
            <DialogActions>
                {
                    showCancelButton && <Button onClick={() => hideModal()}>Cancel</Button>
                }
                {buttons}
            </DialogActions>
        </Dialog>
    );
};
