import React, { useState } from 'react';
import { useModal } from "../../context/ModalContext";
import { MyDialog } from "./MyDialog";
import { DialogContentText, TextField } from "@mui/material";
import { FormWithoutReload } from "../../helpers/FormWithoutReload";
import { useNewFolderMutation } from "../../redux/queries/deviceApi";
import { MyLoadingButton } from "../../helpers/MyLoadingButton";
import { CreateNewFolder } from "@mui/icons-material";
import { useCurrentFolder } from "../../hooks/useCurrentFolder";
import { useFiles } from "../../queries/files/getFiles";

export const NewFolderDialog = () => {
    const currentFolderId = useCurrentFolder();
    const fileQuery = useFiles();
    const { hideModal } = useModal();
    const [folderName, setName] = useState("");
    const [createNewFolder, status] = useNewFolderMutation();

    const onNewFolderButtonClicked = async () =>
        createNewFolder({
            folder_name: folderName,
            parent_folder_id: currentFolderId,
        }).then(hideModal).then(() => fileQuery.refetch());

    return (
        <MyDialog
            title="New Folder"
            content={
                <>
                    <DialogContentText paragraph>
                        Type the folder name below. The new folder will be placed in the currenly open
                        directory.
                    </DialogContentText>
                    <FormWithoutReload onSubmit={onNewFolderButtonClicked}>
                        <TextField
                            fullWidth
                            autoFocus
                            value={folderName}
                            label="Folder name"
                            onChange={(e) => setName(e.currentTarget.value)}
                        />
                    </FormWithoutReload>
                </>
            }
            buttons={
                <MyLoadingButton
                    variant="outlined"
                    disabled={!folderName}
                    startIcon={<CreateNewFolder />}
                    loading={status.isLoading}
                    onClick={onNewFolderButtonClicked}
                >
                    Create
                </MyLoadingButton>
            }
        />
    );
};
