import { EntryType } from "../../queries/files";
import { useModal } from "../../context/ModalContext";
import { getExt } from "../../helpers/filename";
import { ChangeEvent, useEffect, useState } from "react";
import { useRenameFile2 } from "../../queries/files/renameFile";
import { InputAdornment, TextField } from "@mui/material";
import { MyDialog } from "./MyDialog";
import { FormWithoutReload } from "../../helpers/FormWithoutReload";
import { MyLoadingButton } from "../../helpers/MyLoadingButton";
import { DriveFileRenameOutline } from "@mui/icons-material";

export const RenameDialog = ({ file }: { file: EntryType }) => {
    const { hideModal } = useModal();

    const { fileName, ext } = getExt(file.entry_name);
    const [newName, setName] = useState(fileName);
    useEffect(() => setName(fileName), [fileName]);
    const onChange = (e: ChangeEvent<HTMLInputElement>) => setName(e.currentTarget.value);

    const { isLoading, mutateAsync } = useRenameFile2(file);
    const onRenameButtonClicked = async () => {
        await mutateAsync({ newName: newName + ext });
        hideModal();
        return false;
    };

    const endAdornment = ext !== '' && <InputAdornment position="end">{ext}</InputAdornment>;

    return (
        <MyDialog
            title={'Rename file'}
            content={
                <FormWithoutReload onSubmit={onRenameButtonClicked}>
                    <TextField
                        value={newName}
                        onChange={onChange}
                        InputProps={{ endAdornment }}
                    />
                </FormWithoutReload>
            }
            buttons={(
                <MyLoadingButton
                    variant="outlined"
                    startIcon={<DriveFileRenameOutline />}
                    loading={isLoading}
                    onClick={onRenameButtonClicked}
                >
                    Rename
                </MyLoadingButton>
            )}
        />
    );
};
