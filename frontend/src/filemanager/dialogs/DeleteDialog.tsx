import { useModal } from "../../context/ModalContext";
import { useDeleteFile } from "../../queries/files/deleteFile";
import { MyDialog } from "./MyDialog";
import { MyLoadingButton } from "../../helpers/MyLoadingButton";
import { Delete } from "@mui/icons-material";
import { BaseEntryType } from "../BaseEntry";
import { useFiles } from "../../queries/files/getFiles";

export const DeleteDialog = ({ file }: { file: BaseEntryType }) => {
    const { hideModal } = useModal();
    const fileQuery = useFiles();
    const deleteFile = useDeleteFile(file);

    const onDeleteButtonClicked = async () => {
        await deleteFile.mutateAsync();
        hideModal();
    };

    let content;
    if (fileQuery.data?.find(entry => entry.parent_folder_id === file.entry_id)) {
        // there's a file in this folder!
        content = "This folder contains other files! These will also be irreversibly deleted and cannot be recovered.";
    } else {
        content = "The file will be irreversibly deleted from your device and cannot be recovered.";
    }

    return (
        <MyDialog
            title={`Are you sure you want to delete ${file.entry_name}?`}
            content={content}
            buttons={(
                <MyLoadingButton
                    variant="outlined"
                    color="error"
                    loading={deleteFile.isLoading}
                    onClick={onDeleteButtonClicked}
                    startIcon={<Delete />}
                >
                    Delete
                </MyLoadingButton>
            )}
        />
    );
};
