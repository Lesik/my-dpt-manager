import { WaterDamage } from "@mui/icons-material";
import { Box, Typography } from "@mui/material";

export const FileManagerEmpty = () => (
    <Box display='flex' justifyContent='center' alignItems='center' flexDirection='column' height='100%'>
        <WaterDamage fontSize="large" />
        <Typography variant="h5">No files in this folder!</Typography>
        <Typography>Upload a file by dropping it here.</Typography>
    </Box>
);
