import React from 'react';
import { IconButton } from "@mui/material";
import { CreateNewFolder } from "@mui/icons-material";
import { useModal } from "../../context/ModalContext";
import { NewFolderDialog } from "../dialogs/NewFolderDialog";

export const NewFolderButton = () => {
    const { showModal } = useModal();

    return (
        <IconButton onClick={() => showModal(<NewFolderDialog />)}>
            <CreateNewFolder />
        </IconButton>
    );
};
