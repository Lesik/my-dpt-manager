import React from 'react';
import { useFiles } from "../../queries/files/getFiles";
import { IconButton } from "@mui/material";
import { Cached } from "@mui/icons-material";

export const RefreshButton = () => {
  const fileQuery = useFiles();
  return (
      <IconButton onClick={() => fileQuery.refetch()}>
          <Cached />
      </IconButton>
  );
};
