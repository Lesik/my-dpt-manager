import { NavigateNext, TabletAndroid } from "@mui/icons-material";
import { Breadcrumbs as MUIBreadcrumbs, Button } from '@mui/material';
import { useMemo } from "react";
import { Link } from "react-router-dom";
import { getBreadcrumbsForId } from "../../helpers/data";
import { useFiles } from "../../queries/files/getFiles";

export const Breadcrumbs = ({ id }: { id: string }) => {
    const { data: files, status } = useFiles();

    const breadcrumbs = useMemo(() => getBreadcrumbsForId(files || [], id), [files, id]);

    if (status !== 'success') {
        return <p>Loading...</p>;
    }

    return (
        <MUIBreadcrumbs separator={<NavigateNext fontSize="small" />} maxItems={3} sx={{ marginRight: 'auto' }}>
            <Button component={Link} to="/" startIcon={<TabletAndroid />} color="inherit">
                Documents
            </Button>
            {
                breadcrumbs.map(crumb => (
                    <Button
                        key={crumb.entry_id}
                        component={Link}
                        to={`/${crumb.entry_id}`}
                        color="inherit"
                    >
                        {crumb.entry_name}
                    </Button>
                ))
            }
        </MUIBreadcrumbs>
    );
};
