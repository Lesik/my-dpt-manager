import { Button, ButtonProps, CircularProgress } from "@mui/material";

export const MyLoadingButton = ({ loading, ...props }: ButtonProps & { loading: boolean }) => (
    <Button {...props} startIcon={loading ? <CircularProgress size={20} /> : props.startIcon} disabled={loading}>
        {props.children}
    </Button>
);
