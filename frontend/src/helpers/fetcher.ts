async function fetcher<T>(url: string, { body, ...customConfig }: any = {}): Promise<T> {
  const headers = { 'content-type': 'application/json' };
  const config = {
    ...customConfig,
    headers: {
      ...headers,
      ...customConfig.headers,
    },
    credentials: 'include'
  };
  if (body) {
    config.body = JSON.stringify(body);
  }
  const response = await fetch(url, config);
  if (!response.ok) {
    throw new Error(response.status.toString());
  }
  const responseData = await response.json().catch(() => {
    throw new Error('[fetcher] Error parsing response to JSON');
  });
  if (responseData.error) {
    throw new Error(responseData.error.message);
  }
  return responseData as Promise<T>;
}

export default fetcher;
