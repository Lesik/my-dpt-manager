import { CircularProgress, Paper } from "@mui/material";
import { PropsWithChildren } from "react";
import { UseQueryResult } from "react-query";
import { cl } from "./react";
import classes from './RefetchingIndicator.module.css';

export const RefetchingIndicator = ({
    query,
    children,
}: PropsWithChildren<{
    query: UseQueryResult<unknown, unknown>,
}>) => (
    <div className={classes.wrapper}>
        <div className={classes.spinnerContainer}>
            <Paper
                variant="elevation"
                className={cl(classes.spinner, { [classes.fetching]: query.isFetching && query.isSuccess })}
                elevation={3}
                sx={{
                    zIndex: 'tooltip',
                }}
            >
                <CircularProgress variant="indeterminate" size={15} />
            </Paper>
        </div>
        {children}
    </div>
);
