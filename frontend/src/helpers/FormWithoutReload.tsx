import React, { FormEventHandler, HTMLProps } from 'react';

export const FormWithoutReload = ({ onSubmit: onSubmitOriginal, ...rest }: HTMLProps<HTMLFormElement>) => {
    const onSubmit: FormEventHandler<HTMLFormElement> = (event) => {
        event.preventDefault();
        if (onSubmitOriginal)
            onSubmitOriginal(event);
    };

    return <form {...rest} onSubmit={onSubmit} />;
};
