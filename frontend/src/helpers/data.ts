import { EntryType } from "../queries/files";
import { FolderEntryType } from "../filemanager/FolderEntry";

export const findEntryById = (data: EntryType[], id?: string) => (
    data?.find(date => date.entry_id === id)
);

export const findParentById = (data: EntryType[], id?: string) => (
    data?.find(date => date.entry_id === id)
);

export const findParentForId = (data: EntryType[], id: string) => {
    findParentForEntry(data, findEntryById(data, id));
};

export const findParentForEntry = (data: EntryType[], entry?: EntryType) => (
    findParentById(data, entry?.parent_folder_id)
);

export const getBreadcrumbsForId = (data: EntryType[], id: string): EntryType[] => (
    getBreadcrumbsForEntry(data, findEntryById(data, id))
);

export const getBreadcrumbsForEntry = (data: EntryType[], entry?: EntryType): EntryType[] => {
    if (entry === undefined) return [];

    const parent = findParentForEntry(data, entry);
    if (parent === undefined) {
        return [entry];
    }
    return [...getBreadcrumbsForEntry(data, parent), entry];
};

// note to self: never compare by object equality, always by entry.entry_id
export const deleteEntry = (data: EntryType[], entry: EntryType) => data.filter(date => !(date.entry_id === entry.entry_id));

// export const deleteEntryById = (data: EntryType[], entryId: string) => data.filter(date => !(date.entry_id === entryId));

export const renameEntry = (data: EntryType[], entry: EntryType, newName: string) => data.map(date => {
    if (date.entry_id === entry.entry_id) {
        const folder = entry.entry_path.substring(0, entry.entry_path.indexOf(entry.entry_name));
        return {
            ...date,
            entry_name: newName,
            entry_path: folder + newName,
        };
    }
    return date;
});

export const newEntry = (
    data: EntryType[],
    documentId: string,
    fileName: string,
    parentFolder: FolderEntryType,
): EntryType[] => [
    ...data,
    {
        entry_id: "document_id",
        entry_type: "document",
        entry_name: fileName,
        parent_folder_id: parentFolder.entry_id,
        entry_path: parentFolder.entry_path + '/' + fileName,
        document_type: "normal",
        current_page: "1",
        total_page: "1",
        is_new: "true",
        created_date: new Date().toISOString(),
        mime_type: "application/pdf",
        title: fileName,
    },
];
