export const getExt = (fileName: string) => {
    if (fileName.endsWith('.pdf')) {
        const withoutExt = fileName.slice(0, -4);
        return {
            fileName: withoutExt,
            ext: '.pdf',
        };
    }
    return {
        fileName,
        ext: '',
    }
};
