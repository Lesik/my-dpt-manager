import { ListItemButton, ListItemButtonProps, ListItemProps } from "@mui/material";
import classes from './ListItemWithSecondaryAction.module.css';

export const ListItemWithSecondaryAction = ({
    secondaryAction,
    ...props
}: ListItemButtonProps & {
    secondaryAction: ListItemProps['secondaryAction'],
}) => (
    <div className={classes.wrapper}>
        <ListItemButton {...props} sx={{ height: '100%' }} />
        <div className={classes.secondaryAction}>
            { secondaryAction }
        </div>
    </div>
);

/**
 * Old version, with actions as array of objects instead of just component passed. Didn't work
 * because IconButton's action prop didn't like MouseEventHandler<HTMLButtonEvent>, but it is needed
 * by some secondary actions to access currentElement...
 */

// type SecondaryAction = {
//     icon: SvgIconComponent,
//     tooltip: string,
//     action: MouseEventHandler<HTMLButtonElement>,
// };

// export const ListItemWithSecondaryAction = ({
//     secondaryActions,
//     ...props
// }: Omit<ListItemProps, 'secondaryAction'> & {
//     secondaryActions: SecondaryAction[]
// }) => (
//     <div style={{
//         display: 'flex',
//         position: 'relative',
//     }}>
//         <ListItem {...props} />
//         <div className={classes.secondaryActions}>
//             {
//                 secondaryActions.map(secondaryAction => (
//                     <IconButton action={secondaryAction.action} >
//                         <Icon component={secondaryAction.icon} />
//                     </IconButton>
//                 ))
//             }
//         </div>
//     </div>
// );
