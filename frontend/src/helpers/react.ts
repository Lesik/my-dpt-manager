/*
 * Helps with className property when there are multiple or conditional
 * Loosely based on https://github.com/JedWatson/classnames
 *
 * Use like this:
 * ```tsx
 * import classes from './MyComponent.module.css';
 *
 * cl("alwaysOn", ["all" "of", "these", "alwaysOn"], {
 *   conditionalOn: shouldClassBeOn(),
 *   [classes.myConditionalClass]: shouldClassBeOn,
 * }))
 * ```
 */

import { Context, useContext, useState } from "react";

export const cl = (...args: any[]): string => {
  const classes: string[] = [];

  args.forEach((arg) => {
    // safeguard for undefined values
    if (typeof arg === 'undefined' || arg === null || arg === '') {
      return;
    }

    if (typeof arg === 'string') {
      classes.push(arg);
    } else if (Array.isArray(arg)) {
      classes.push(cl(...arg));
    } else if (typeof arg === 'object') {
      Object.entries(arg).forEach(([key, value]) => {
        if (value) {
          classes.push(key);
        }
      });
    }
  });

  return classes.join(' ');
}

export const useBooleanState = (initialValue: boolean | (() => boolean)) => {
  const [value, setValue] = useState(initialValue);
  const setTrue = () => setValue(true);
  const setFalse = () => setValue(false);
  const invertValue = () => setValue(!value);
  return {
    value, setTrue, setFalse, invertValue, setValue,
  };
}

export const useContextSafely = <T>(context: Context<T>) => {
  const displayName = context.displayName || 'Context';
  const usedContext = useContext(context);
  if (Object.keys(usedContext).length === 0) {
    throw new Error(`${displayName} is empty, did you forget to wrap with <${displayName}.Provider>?`);
  }
  return usedContext;
}
