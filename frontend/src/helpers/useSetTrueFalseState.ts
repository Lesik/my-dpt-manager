import { useState } from "react";

export const useSetTrueFalseState = (initialValue: boolean): [
    boolean,
    () => void,
    () => void,
] => {
    const [state, setState] = useState(initialValue);
    return [
        state,
        () => setState(true),
        () => setState(false),
    ];
}
