import { BaseQueryFn, createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';
import { AppStore } from "../appStore";

const dynamicBaseQuery: BaseQueryFn = async (args, api, extraOptions) => {
    const state = api.getState() as AppStore;
    const { ip } = state.deviceConnection;

    const rawBaseQuery = fetchBaseQuery({ baseUrl: `/proxy/${ip}` });
    return rawBaseQuery(args, api, extraOptions);
};

export const deviceApi = createApi({
    reducerPath: 'deviceApi',
    baseQuery: dynamicBaseQuery,
    endpoints: (builder) => ({
        newFolder: builder.mutation<unknown, {
            folder_name: string,
            parent_folder_id: string,
        }>({
            query: (args) => ({
                url: '/folders2',
                method: "POST",
                body: args,
            }),
        }),
    }),
});

export const { useNewFolderMutation } = deviceApi;
