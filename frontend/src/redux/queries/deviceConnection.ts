import { createSlice, PayloadAction } from "@reduxjs/toolkit";

const initialState: {
    ip?: string,
} = {
    ip: undefined,
};

const deviceConnectionSlice = createSlice({
    name: 'deviceConnection',
    initialState,
    reducers: {
        setDevice: (state, action: PayloadAction<string>) => {
            state.ip = action.payload;
        },
    },
});

export const { reducer: deviceConnectionReducer, name: deviceConnectionName } = deviceConnectionSlice;
export const { setDevice } = deviceConnectionSlice.actions;
