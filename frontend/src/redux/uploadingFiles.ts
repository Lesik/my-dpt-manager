import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { UploadingFile } from "../types";

const initialState: {
    files: UploadingFile[],
} = {
    files: [],
};

const findFileById = (files: UploadingFile[], id: string) => files.find(file => file.document_id === id);

export const uploadingFilesSlice = createSlice({
    name: 'uploadingFilesNew',
    initialState,
    reducers: {
        addUploadingFile: (state, action: PayloadAction<UploadingFile>) => {
            state.files.push(action.payload);
        },
        setUploadingProgress: (state, action: PayloadAction<{
            document_id: string,
            newProgress: number,
        }>) => {
            const file = findFileById(state.files, action.payload.document_id);
            if (file) file.progress = action.payload.newProgress;
        },
        uploadingComplete: (state, action: PayloadAction<{ document_id: string }>) => {
            const file = findFileById(state.files, action.payload.document_id);
            if (file) file.completed = true;
        },
        clearUploads: (state) => {
            state.files = [];
        },
    },
});

export const { reducer } = uploadingFilesSlice;

