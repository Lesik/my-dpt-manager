import { useDeviceConnection } from "../context/DeviceConnectionContext";
import { Dispatch, useDispatch, useSelector } from "./appStore";
import { uploadingFilesSlice } from "./uploadingFiles";
import { useGetFiles2 } from "../queries/files/getFiles";

export const useUploadingFiles = () => useSelector(state => state.uploadingFilesNew);
export const useNewUploadFile = () => {
    const { axiosInst } = useDeviceConnection();
    const dispatch = useDispatch();
    const getFiles = useGetFiles2();

    // @ts-ignore
    return (file: File, parentFolderId: string) => dispatch(async (dispatch: Dispatch) => {
        type PostResponse = {
            document_id: string,
        }

        const { data } = await axiosInst.post<PostResponse>('/documents2', {
            file_name: file.name,
            parent_folder_id: parentFolderId,
            document_source: "", // TODO: what is this?
        });

        const { document_id } = data;

        dispatch(uploadingFilesSlice.actions.addUploadingFile({
            fileName: file.name,
            progress: 0,
            document_id,
            completed: false,
        }));

        const formData = new FormData();
        formData.append(file.name, file);

        await axiosInst.put(`/documents/${document_id}/file`, formData, {
            onUploadProgress: (event) => {
                const newProgress = Math.floor((event.loaded * 100) / event.total);
                dispatch(uploadingFilesSlice.actions.setUploadingProgress({ document_id, newProgress }));
            },
        });

        dispatch(uploadingFilesSlice.actions.uploadingComplete({
            document_id,
        }));

        await getFiles.invalidateQuery();
    });
};
