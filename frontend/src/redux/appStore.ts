import { configureStore } from "@reduxjs/toolkit";
import { TypedUseSelectorHook, useDispatch as useReduxDispatch, useSelector as useReduxSelector } from "react-redux";
import { reducer } from "./uploadingFiles";
import { deviceApi } from "./queries/deviceApi";
import { deviceConnectionName, deviceConnectionReducer } from "./queries/deviceConnection";

export const appStore = configureStore({
    reducer: {
        uploadingFilesNew: reducer,
        [deviceConnectionName]: deviceConnectionReducer,
        [deviceApi.reducerPath]: deviceApi.reducer,
    },
    middleware: (getDefaultMiddleware => getDefaultMiddleware().concat(
        deviceApi.middleware,
    )),
});

export type AppStore = ReturnType<typeof appStore.getState>;
export type Dispatch = typeof appStore.dispatch;

export const useDispatch = () => useReduxDispatch<Dispatch>();
export const useSelector: TypedUseSelectorHook<AppStore> = useReduxSelector;
