import { DocumentEntryType } from "../../filemanager/DocumentEntry";
import { FolderEntryType } from "../../filemanager/FolderEntry";
import { deleteEntry, newEntry, renameEntry } from "../../helpers/data";

export type EntryType = DocumentEntryType | FolderEntryType;

export const fakeDeleteFile = (file: EntryType) => fakeFiles = deleteEntry(fakeFiles, file);

export const fakeRenameFile = (file: EntryType, newName: string) => fakeFiles = renameEntry(fakeFiles, file, newName);

export const fakeInsertFile = (documentId: string, fileName: string, parentFolder: FolderEntryType) => (
    fakeFiles = newEntry(fakeFiles, documentId, fileName, parentFolder)
);

export let fakeFiles: EntryType[] = [
    {
        author: "Ritchie, D.M. and Kernighan, B.W.",
        created_date: "2021-01-25T14:44:09Z",
        current_page: "1",
        document_type: "normal",
        entry_id: "my-id",
        entry_name: "The C Programming language.pdf",
        entry_path: "Document/The C Programming Language.pdf",
        entry_type: "document",
        file_revision: "a1295beb4204.1.0",
        file_size: "5857681",
        is_new: "false",
        mime_type: "application/pdf",
        parent_folder_id: "7f922b78-534e-497b-a51a-b3f0af099ccc",
        title: "The C Programming Language",
        total_page: "585"
    },
    {
        created_date: "2021-01-25T14:43:42Z",
        entry_id: "george-orwell-directory-id",
        entry_name: "George Orwell",
        entry_path: "Document/George Orwell",
        entry_type: "folder",
        ext_id: "",
        is_new: "false",
        parent_folder_id: "root"
    },
    {
        author: "Orwell, George",
        created_date: "2021-01-25T14:44:09Z",
        current_page: "1",
        document_type: "normal",
        entry_id: "animal-farm-file-id",
        entry_name: "Animal Farm.pdf",
        entry_path: "Document/George Orwell/Animal Farm.pdf",
        entry_type: "document",
        file_revision: "a1295beb4204.1.0",
        file_size: "5857681",
        is_new: "false",
        mime_type: "application/pdf",
        parent_folder_id: "george-orwell-directory-id",
        title: "Animal Farm.pdf",
        total_page: "585"
    },
    {
        created_date: "2021-01-25T14:43:42Z",
        entry_id: "empty-folder",
        entry_name: "Empty Folder",
        entry_type: "folder",
        entry_path: "Document/Empty Folder",
        parent_folder_id: "george-orwell-directory-id",
        ext_id: "",
        is_new: "false",
    },
    {
        created_date: "2021-01-25T14:43:42Z",
        entry_id: "empty-folderasadasd",
        entry_name: "Folder with a really really really really really long name!!!!!!!",
        entry_type: "folder",
        entry_path: "Document/Empty Folder",
        parent_folder_id: "george-orwell-directory-id",
        ext_id: "",
        is_new: "false",
    },
];
