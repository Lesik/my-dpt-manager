import { useQuery, useQueryClient } from "react-query";
import { DocumentEntryType } from "../../filemanager/DocumentEntry";
import { FolderEntryType } from "../../filemanager/FolderEntry";
import { EntryType } from '.';
import { useDeviceConnection } from "../../context/DeviceConnectionContext";

type FileResponse = {
    count: number,
    entry_list: EntryType[],
    entry_list_hash: string,
};

export const QUERY_GET_FILES = 'files';

const path = '/documents2?entry_type=all';

export const useFiles = () => {
    const { axiosInst } = useDeviceConnection();

    const fetchFiles = async () => (await axiosInst.get<FileResponse>(path)).data.entry_list;

    return useQuery(QUERY_GET_FILES, fetchFiles);
};

export const useGetFiles2 = () => {
    const key = 'files';
    type DataType = (DocumentEntryType | FolderEntryType)[];

    const { axiosInst } = useDeviceConnection();
    const queryClient = useQueryClient();

    const fetchFiles = async () => (await axiosInst.get<FileResponse>(path)).data.entry_list;

    return {
        useQuery: () => useQuery(key, fetchFiles),
        invalidateQuery: () => queryClient.invalidateQueries(key),
        getQueryData: () => queryClient.getQueryData<DataType>(key),
        setQueryData: (data: DataType) => (
            queryClient.setQueryData<DataType>(key, data)
        ),
        changeQueryData: (changeFunc: (data: DataType) => DataType) => {

        },
    };
};
