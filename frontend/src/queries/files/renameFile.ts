import { DocumentEntryType } from "../../filemanager/DocumentEntry";
import { useMutation } from "react-query";
import { useGetFiles2 } from "./getFiles";
import { FolderEntryType } from "../../filemanager/FolderEntry";
import { EntryType } from "./index";
import { useDeviceConnection } from "../../context/DeviceConnectionContext";

type Params = {
  newName: string,
};

type Context = {
  oldData: (DocumentEntryType | FolderEntryType)[] | undefined,
};

export const useRenameFile2 = (file: EntryType) => {
    const { axiosInst } = useDeviceConnection();
    const getFiles = useGetFiles2();

  return useMutation<unknown,
      unknown,
      Params,
      Context>(
      ({ newName }) => axiosInst.put(`/documents/${file.entry_id}`, {
        parent_folder_id: file.parent_folder_id,
        file_name: newName,
      }), {
          onSuccess: () => getFiles.invalidateQuery(),
      });
};
