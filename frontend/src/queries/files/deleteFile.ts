import { useMutation } from "react-query";
import { useGetFiles2 } from "./getFiles";
import { useDeviceConnection } from "../../context/DeviceConnectionContext";
import { BaseEntryType } from "../../filemanager/BaseEntry";


export const useDeleteFile = (file: BaseEntryType) => {
    const { axiosInst } = useDeviceConnection();
    const files = useGetFiles2();

    let endpoint: string;
    switch (file.entry_type) {
        case "document":
            endpoint = `/documents/${file.entry_id}`;
            break;
        case "folder":
            endpoint = `/folders/${file.entry_id}`;
            break;
    }

    const deleteFile = () => axiosInst.delete(endpoint);

    return useMutation(deleteFile, {
        onSuccess: () => files.invalidateQuery(),
    });
};
