import fetcher from "../../helpers/fetcher";
import { useQuery } from "react-query";

export type InformationResponse = {
    modelName: string,
    deviceColor: string,
    serialNumber: string,
};

const getDeviceInformation = (ip: string) => () => fetcher<InformationResponse>(`/information/${ip}`);
export const useDeviceInformation = (ip: string) => useQuery("deviceInformation", getDeviceInformation(ip));
