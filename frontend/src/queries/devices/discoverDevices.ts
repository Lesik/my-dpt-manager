import { useQuery } from "react-query";
import fetcher from "../../helpers/fetcher";

export type DiscoverResponse = {
    ip: string,
}

const fetchDevices = () => fetcher<DiscoverResponse>('/discover');
export const useDiscoveredDevices = () => useQuery('discover', fetchDevices, {
    retry: 0,
});
