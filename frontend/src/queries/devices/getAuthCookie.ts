import fetcher from "../../helpers/fetcher";
import { useQuery } from "react-query";
import { useDeviceSetupContext } from "../../context/DeviceConnectionContext";
import { useDispatch } from "../../redux/appStore";
import { setDevice } from "../../redux/queries/deviceConnection";

export type AuthCookieResponse = {
    cookie: string,
};

const getAuthCookie = (ip: string) => () => fetcher<AuthCookieResponse>(`/authcookie/${ip}`);

export const useAuthCookie = (isEnabled: boolean, ip: string) => {
    const { setDevice: setDeviceReactQuery } = useDeviceSetupContext();
    const dispatch = useDispatch();

    const onSuccess = () => {
        setDeviceReactQuery({ ip });
        dispatch(setDevice(ip));
    };

    return useQuery(['authcookie', ip], getAuthCookie(ip), {
        enabled: isEnabled,
        cacheTime: 0,
        staleTime: 0,
        onSuccess,
    });
};
