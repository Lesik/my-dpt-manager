import {DiscoverResponse} from "./discoverDevices";
import {InformationResponse} from "./getDeviceInformation";
import {AuthCookieResponse} from "./getAuthCookie";

export const fakeDevices: DiscoverResponse = {
    ip: '0.0.0.0',
}

export const fakeDeviceInformation: InformationResponse = {
    modelName: 'Test Device',
    deviceColor: 'rainbow',
    serialNumber: 'foobar',
}

export const fakeAuthCookie: AuthCookieResponse = {
    cookie: 'FOO-BAR',
}
