import React, { PropsWithChildren } from "react";
import classes from "./Carousel.module.css";

export function Carousel({
    children,
}: PropsWithChildren<{}>) {
    return (
        <div className={classes.container}>
            {children}
        </div>
    )
}
