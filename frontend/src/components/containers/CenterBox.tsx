import React, { PropsWithChildren } from "react";
import { cl } from "../../helpers/react";
import classes from "./CenterBox.module.css";

export function CenterBox({ children, direction = "row" }: PropsWithChildren<{
    direction?: "row" | "column",
}>) {
    return <div className={cl(classes.container, classes[direction])}>{ children }</div>;
}
