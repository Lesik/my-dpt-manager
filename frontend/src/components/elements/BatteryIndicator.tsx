import { Battery20, Battery30, Battery50, Battery60, Battery80, Battery90, BatteryAlert, BatteryCharging20, BatteryCharging30, BatteryCharging50, BatteryCharging60, BatteryCharging80, BatteryCharging90, BatteryChargingFull, BatteryFull, BatteryUnknown } from "@mui/icons-material";
import React from "react";
import { useQuery } from "react-query";
import fetcher from "../../helpers/fetcher";

type BatteryResponse = {
    level: number,
    charging: boolean,
};

const fetch = () => fetcher<BatteryResponse>('/system/status/battery');

export const BatteryIndicator = () => {
    const query = useQuery(['battery'], fetch, {
        staleTime: 5 * 60,
    });

    if (query.isError || query.isLoading || query.isFetching || query.isIdle) {
        return <BatteryUnknown />;
    }

    const charging = query.data.charging;
    switch (query.data.level) {
        case 0:
            return <BatteryAlert />;
        case 20:
            return charging ? <BatteryCharging20/> : <Battery20 />;
        case 30:
            return charging ? <BatteryCharging30 /> : <Battery30 />;
        case 50:
            return charging ? <BatteryCharging50 /> : <Battery50 />;
        case 60:
            return charging ? <BatteryCharging60 /> : <Battery60 />;
        case 80:
            return charging ? <BatteryCharging80 /> : <Battery80 />;
        case 90:
            return charging ? <BatteryCharging90 /> : <Battery90 />;
        case 100:
            return charging ? <BatteryChargingFull /> : <BatteryFull />;
    }
};
