import React from 'react';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import { FileManagerView } from './views/FileManagerView';
import { DeviceConnectionContextProvider } from "./context/DeviceConnectionContext";
import { ActionBar } from "./actionbar/ActionBar";

function App() {
    const fmview = <FileManagerView />;

    return (
        <DeviceConnectionContextProvider>
            <Router>
                <ActionBar />
                <Routes>
                    <Route path="/" element={fmview} />
                    <Route path="/:id" element={fmview} />
                </Routes>
            </Router>
        </DeviceConnectionContextProvider>
    );
}

export default App;
