<img alt="App logo" src="./package/app-icon.png" align="right" />

# My DPT Manager

My DPT Manager aims to be a user-friendly graphical application for managing your Sony Digital Paper Tablet (DPT). 
You can use it to upload files, create folders, as well as renaming and deleting them. More functionality, such as 
changing settings or uploading templates, is planned.

### Status

The application is working well and being used daily. But you should you use it at your own risk.

### Compatibility

I am testing this application with a Sony DPT-CP1, however other models such as the DPT-RP1 should work as well.

### Installation

My DPT Manager is available as Flatpak. To install, add the repo through a GUI or by running:

```shell
$ flatpak remote-add my-dpt-manager-remote https://repo.lesik.dönerbu.de/lesik.flatpakrepo
```

Afterwards, you can install My DPT Manager through your software app or by running:

```shell
$ flatpak install io.gitlab.lesik.MyDptManager
```

### Usage

After installing, you should find My DPT Manager in your list of applications.

![Launching My DPT Manager](./screenshots/launcher.png)

Upon running it, the app will open and automatically start searching for DPT devices in your network. Once a device 
has been found, you will be prompted to confirm the connection:

![Connecting to a device](./screenshots/device-selection.png)

After connecting, you will see a list of files on the device:

![List of files on device](./screenshots/list-of-files.png)

You can click on a folder to show the files inside it. Additional actions, such as renaming or deleting a file, are 
available through the three-dot icon on the right of the file:

![Three dot menu](./screenshots/three-dot-menu.png)

![Renaming a file](./screenshots/renaming-file.png)

![Deleting a file](./screenshots/deleting-file.png)

### Technical setup (skip this is you're not a developer)

First, how to build and run.

```shell
$ npm install
$ npm run build
$ node build/backend.bundle.js
```

The application consists of a React frontend and an Express backend. This is because a browser app alone is too 
limited for direct communication with the DPT:

- To discover a DPT in the network, the mDNS protocol is used, which is not supported in browser.
- After discovery, further communication occurs through HTTPS, with the DPT offering a self-signed certificate, 
  which a browser would reject.

The backend consists of several parts:

#### Device communication

Inside the `device/` folder are the endpoints responsible to communicating with the DPT.

The `/discovery` endpoint uses Bonjour to locate DPT devices in the local network that broadcast mDNS records.

The `/information` is the only endpoint that communicates to the device over HTTP. It fetches general information 
about the device, such as model and serial number.

The `/authcookie` endpoint is responsible for retrieving an authentication token that will be used for all 
further communication to the device. It does by first retrieving a nonce that it has to sign with a private key 
already known to the device. At this moment, registering a new private key is not yet possible in this application.

Further communication with the device is facilitated by the `/proxy` endpoint, which simply redirects all requests 
to the DPT, stripping the `/proxy` from the URL. Requests made to this endpoint are expected to carry the 
authentication token (in the form of a cookie) described above. Importantly, the backend will not hold onto the 
token for you, this must be done by the caller (i.e. the frontend);

Currently, the `/proxy` does not authenticate requests, so any application on your computer could use this endpoint to
send random requests to your DPT. However, I believe the practical risk to be relatively low, since if you have 
software running on your computer that makes random requests without your consent, you have a much bigger problem.

#### Mock

For testing purposes, the endpoints described in the device section above have a mocked counterpart (i.e. delivering 
fake data). Currently, not all endpoints that the DPT offers are mocked; this will likely never be the case, as the 
total list of them is unknown due to a lack of documentation from Sony.

#### Keepalive

When you run the application, you are starting up an Express server running on NodeJS. After the server has started, 
a request to open a browser window is sent to your operating system, pointing to the URL of the server. The server 
will then serve the actual application (frontend) to the browser and provide the endpoints necessary for 
communication with the device.

However, if the browser window is closed again, the Express server would still be running. To prevent this, a 
WebSocket (WS) server is started simultaneously to the HTTP server. Upon loading, the frontend will try to connect to 
the WS, which will increase a counter inside the backend. Should the browser window be closed, the WS connection is 
dropped automatically by the browser, upon which the counter will be decremented. Should the counter reach 0 
(meaning all applications have been closed), the backend is not needed anymore and the server will shut itself down.

This is a clever way (I think) that I came up with as an alternative to Electron, which is usually used for desktop 
web apps instead. Electron apps come with a massive overhead in performance, application size and technical debt. My 
approach uses technologies already built into the browser that most developers are already familiar with, are performant
and do not increase the bundle size.   

#### React

The frontend of the app is written in React with functional components only.

Those components that I tackled in the very beginning, such as the `FileManagerView`, have been written at a time 
when I was still very new to React, and don't accurately represent my coding abilities (I didn't get around to 
refactoring them yet, sadly, due to a lack of time). Components that I tackled last, such as the dialogs (under 
`filemanager/dialogs`), do me better justice in that regard.

While JavaScript's flexibility is great for quick prototyping, TypeScript is an indispensable tool for any serious 
project (which this strives to be :). Particularly during communication with third-party APIs, like the DPT in this 
case, good type definitions ensure that you don't access invalid properties due to unfamiliarity with the API.

The MUI library provides common app components such as lists, menus and dialogs. It is an exhaustive library with 
a large community and extensive customization possibilities, but in hindsight slightly overkill for this small project. 
Especially the hard dependency on the emotion library, which I don't use in my own code, is quite a thorn.

Initially this project used React-Query to provide common networking features in a nice React hook. During 
development, I decided to try out Redux together with Redux Toolkit, and found that it works nicer with TypeScript. 
Due to a lack of time, both libraries are used for different network calls at the moment, but the goal is to replace 
everything with Redux Toolkit.

For styling, CSS modules are used. They are natively supported by react-scripts, and offer encapsulation of styling 
between components by generating random class names for each. Compared to other common choices such as CSS-in-JS, 
CSS modules are simpler, are natively supported, and because it uses just regular plain CSS, it's easy get started 
(low learning curve) and to re-use styles from or in projects that aren't written in React.
