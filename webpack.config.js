const path = require('path');
const webpack = require("webpack");

const frontend = (_, options) => ({
    name: 'frontend',
    target: 'web',
    entry: './frontend/src/index.tsx',
    output: {
        path: path.resolve(__dirname, 'build/'),
        filename: 'frontend.bundle.js',
    },
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                use: ['ts-loader'],
            },
            {
                test: /\.module\.css$/,
                use: [
                    {
                        loader: 'style-loader',
                        options: {
                            injectType: 'singletonStyleTag',
                        },
                    },
                    {
                        loader: 'css-loader',
                        options: {
                            modules: {
                                localIdentName: options.mode === 'development' ? "[name]__[local]--[hash:base64:5]" : "[hash:base64:5]",
                            },
                        },
                    },
                ],
            },
        ],
    },
    resolve: {
        extensions: ['.js', '.ts', '.tsx'],
    },
    plugins: [
        // need to import frontend as one file to serve from backend
        new webpack.optimize.LimitChunkCountPlugin({
            maxChunks: 1
        })
    ],
});

const backend = {
    name: 'backend',
    target: 'node',
    dependencies: ['frontend'],
    entry: path.resolve(__dirname, './backend/src/index.ts'),
    output: {
        path: path.resolve(__dirname, 'build/'),
        filename: 'backend.bundle.js',
    },
    module: {
        rules: [
            {
                test: /\.ts$/,
                use: ['ts-loader'],
            },
            {
                test: /\.bundle.js$/,
                type: 'asset/source',
            }
        ],
    },
    resolve: {
        extensions: ['.js', '.ts', '.tsx'],
    },
};

module.exports = [backend, frontend];
