import express from "express";
import { deviceDiscoveryRouter } from "./device/discovery";
import { deviceProxyRouter } from "./device/proxy";
import { serveHtmlRouter } from "./servehtml";
import { IS_MOCK, PORT } from "./globals";
import { mockDiscoveryRouter } from "./mock/discovery";
import { mockProxyRouter } from "./mock/proxy";
import { setupKeepAlive } from "./keepalive";
import { setupXdgOpen } from "./xdgopen";

export const expressApp = express();

if (IS_MOCK) {
    expressApp.use(mockDiscoveryRouter);
    expressApp.use(mockProxyRouter);
} else {
    expressApp.use(deviceDiscoveryRouter);
    expressApp.use(deviceProxyRouter);
}

expressApp.use(serveHtmlRouter);

const httpServer = expressApp.listen(PORT, () => {
    setupKeepAlive(httpServer);
    setupXdgOpen(httpServer);
    console.log(`Server running on port ${PORT}.`);
});
