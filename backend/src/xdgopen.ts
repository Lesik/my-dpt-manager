import { Server } from "http";
import { spawn } from "child_process";
import { PORT } from "./globals";

export const setupXdgOpen = (server: Server) => {
    const url = `http://localhost:${PORT}`;

    // TODO: won't work on non-Linux
    spawn('xdg-open', [url]);
};
