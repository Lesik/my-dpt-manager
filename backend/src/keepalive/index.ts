import { Server } from 'http';
import { Server as WebSocketServer } from 'ws';
import { exit } from "process";

let connectionCount = 0;

export const setupKeepAlive = (server: Server) => {
    const wsServer = new WebSocketServer({ server });

    wsServer.on('connection', (ws) => {
        connectionCount++;
        console.log(`New browser connected through websocket. Active browsers: ${connectionCount}`);

        ws.on('close', () => {
            connectionCount--;
            if (connectionCount === 0) {
                console.log("Active connections dropped to 0, exiting...");
                server?.close();
                exit();
            }
        });
    });
};
