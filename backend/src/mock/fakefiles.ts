export const fakeFiles: {
    author?: string,
    entry_id: string,
    entry_name: string,
    entry_type?: "document" | "folder",
    parent_folder_id: string,
    title?: string,
}[] = [
    {
        author: "Ritchie, D.M. and Kernighan, B.W.",
        entry_id: "my-id",
        entry_name: "The C Programming language.pdf",
        entry_type: "document",
        parent_folder_id: "root",
    },
    {
        entry_id: "george-orwell-directory-id",
        entry_name: "George Orwell",
        entry_type: "folder",
        parent_folder_id: "root",
    },
    {
        author: "Orwell, George",
        entry_id: "animal-farm-file-id",
        entry_name: "Animal Farm.pdf",
        entry_type: "document",
        parent_folder_id: "george-orwell-directory-id",
        title: "Animal Farm.pdf",
    },
    {
        entry_id: "empty-folder",
        entry_name: "Empty Folder",
        entry_type: "folder",
        parent_folder_id: "george-orwell-directory-id",
    },
    {
        entry_id: "empty-folderasadasd",
        entry_name: "Folder with a really really really really really long name",
        entry_type: "folder",
        parent_folder_id: "george-orwell-directory-id",
    },
];
