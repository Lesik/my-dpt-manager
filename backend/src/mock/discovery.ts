import { Router } from "express";
import { sleep } from "../helpers/sleep";
import { mockProxyRouter } from "./proxy";
import { DELAY_IN_MS } from "../globals";

export const mockDiscoveryRouter = Router();

mockDiscoveryRouter.use(async (req, res, next) => {
    await sleep(DELAY_IN_MS);
    next();
});

mockDiscoveryRouter.get('/discover', (_, res) => res.json({
    ip: '1337.42.1337.42',
}));

mockDiscoveryRouter.get('/information/:ip', (_, res) => res.json({
    modelName: 'Test Device',
    color: 'rainbow',
    serialNumber: '133742133742',
}));

mockDiscoveryRouter.get('/authcookie/:ip', async ({ params: { ip } }, res) => res.json({
    success: true,
}));

mockDiscoveryRouter.use('/proxy/:ip', mockProxyRouter);
