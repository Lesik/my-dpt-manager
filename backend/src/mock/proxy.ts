import express, { Router } from "express";
import { fakeFiles as fakeFilesOriginal } from './fakefiles';

let fakeFiles = fakeFilesOriginal;

export const mockProxyRouter = Router();
mockProxyRouter.use(express.json());

let tempFiles: {
    document_id: string,
    file_name: string,
    parent_folder_id: string,
}[] = [];

mockProxyRouter.post('/documents2', (req, res) => {
    const document_id = `dummy-document-id-${Math.random()}`;
    tempFiles = [...tempFiles, {
        document_id,
        file_name: req.body.file_name,
        parent_folder_id: req.body.parent_folder_id,
    }];
    return res.json({ document_id });
});

mockProxyRouter.put('/documents/:id/file', (req, res) => {
    const document_id = req.params.id;
    const file = tempFiles.find(file => file.document_id === document_id);
    if (!file) return res.status(500).json({ error: `document_id '${document_id}' not known, upload first` });
    const { file_name, parent_folder_id } = file;
    fakeFiles.push({
        entry_id: document_id,
        entry_type: 'document',
        entry_name: file_name,
        parent_folder_id,
    });
    res.json({ success: true });
});

mockProxyRouter.get('/documents2', (req, res) => res.json({
    entry_list: fakeFiles,
}));

mockProxyRouter.delete('/documents/:id', (req, res) => {
    fakeFiles = fakeFiles.filter(file => file.entry_id !== req.params.id);
    res.status(204).json({
        success: true,
    });
});

mockProxyRouter.put('/documents/:id', (req, res) => {
    const { file_name, parent_folder_id } = req.body;
    fakeFiles = fakeFiles.map(file => {
        if (file.entry_id === req.params.id) {
            return {
                ...file,
                entry_name: file_name,
                parent_folder_id,
            };
        }
        return file;
    });
    res.json({
        success: true,
    });
});

mockProxyRouter.post('/folders2', (req, res) => {
    const { folder_name, parent_folder_id } = req.body;
    fakeFiles.push({
        entry_type: 'folder',
        entry_name: folder_name,
        parent_folder_id,
        entry_id: `new-folder-${Math.random()}`,
    });
    res.json({
        success: true,
    });
});
