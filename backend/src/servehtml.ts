import { Router } from "express";
import { spawn } from "child_process";

export const serveHtmlRouter = Router();

try {
    const frontend = require("../../build/frontend.bundle.js");
    serveHtmlRouter.get('/', (req, res) => res.send(`
        <html>
        <body>
        <div id="root"></div>
        <script>${frontend}</script>
        </body>
        </html>
    `));
} catch {
    serveHtmlRouter.get('/', (req, res) => (
        res.sendStatus(404).send(`
<h1>Sorry!</h1>
<p>Can't serve the frontend as it isn't built yet. Are you running the right webpack config?</p>
      `)
    ));
}
