import bonjour from "bonjour";
import axios from "axios";
import path from "path";
import os from "os";
import { promises as fs } from "fs";
import https from "https";
import crypto from "crypto";
import { Router } from "express";
import { sleep } from "../helpers/sleep";

export const deviceDiscoveryRouter = Router();

const numberOfTries = 7;

deviceDiscoveryRouter.get('/discover', async (req, res) => {
    let response;

    const browser = bonjour().findOne({ type: 'digitalpaper' }, (service) => response = {
        ip: service.addresses[0],
        rest: service,
    });

    await sleep(2_000);
    let i = 0;
    for (let i = 0; (i < numberOfTries && !response); i++) {
        browser.update();
        console.log(`No devices found yet, retrying (try ${i + 1} of ${numberOfTries})`);
        await sleep(5_000);
    }

    res.json(response);
});

deviceDiscoveryRouter.get('/information/:ip', async ({ params: { ip } }, res) => {
    const url = `http://${ip}:8080/register/information`;
    try {
        const information = await axios.get(url);
        res.json({
            modelName: information.data["model_name"],
            deviceColor: information.data["device_color"],
            serialNumber: information.data["serial_number"],
        });
    } catch (e) {
        res.json({
            error: e,
        });
    }
});

deviceDiscoveryRouter.get('/authcookie/:ip', async ({ params: { ip } }, res) => {
    const dir = path.join(os.homedir(), '.dpapp');

    try {
        const clientId = (await fs.readFile(path.join(dir, `/deviceid.dat`))).toString();
        const privateKey = await fs.readFile(path.join(dir, `/privatekey.dat`));

        const instance = axios.create({
            baseURL: `https://${ip}:8443`,
            httpsAgent: new https.Agent({
                rejectUnauthorized: false,
            }),
        });

        const nonce = await instance.get(`/auth/nonce/${clientId}`);

        const sign = crypto.createSign("SHA256");
        sign.write(nonce.data.nonce);
        sign.end();
        const nonceSigned = sign.sign(privateKey, 'base64');

        const auth = await instance.put("/auth", {
            clientId,
            nonceSigned,
        });

        const match = auth.headers["set-cookie"][0].match(/Credentials=[a-zA-Z0-9\/+]+?;/)[0];
        if (!match) {
            return res.status(500).json({ error: { code: 1337 } });
        }
        res.setHeader('Set-Cookie', `${match} Path=/; HttpOnly`);
        res.json({
            success: true,
        });
    } catch (e) {
        console.error(e);
        res.status(500).json({
            error: {
                code: e,
                source: axios.isAxiosError(e) ? 'axios' : 'node',
            },
        });
    }
});
