import { createProxyMiddleware } from "http-proxy-middleware";
import { Router } from "express";

export const deviceProxyRouter = Router();

deviceProxyRouter.use('/proxy/:ip/*', createProxyMiddleware({
    // onProxyReq: (proxyReq, req) => {
    //     proxyReq.setHeader('Cookie', req.params.ip);
    // },
    router: (req) => ({
        protocol: 'https:',
        host: req.params.ip,
        port: 8443,
    }),
    pathRewrite: (path, req) => path.replace(`/proxy/${req.params.ip}`, ''),
    onProxyReq: (proxyReq, req, res) => console.log('proxying', proxyReq, req, res),
    secure: false,
    onError: (err, _, res) => res.status(500).json({ error: err }),
}));
